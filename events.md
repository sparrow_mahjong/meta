This document includes all events we need to keep track of

- Turn Change
  * Includes time left
  * Whose players turn it is

- Pung (with cards punged)
- Chow (with cards chowed)
- Kong (with cards konged)

- Mahjong (with all cards)
- Game End (point change)

- Discard (with card discarded)
- Draw


