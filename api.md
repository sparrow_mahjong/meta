# Sparrow API

## Lobby API

- `POST /login -> AuthToken`
  * Authenticate your current session
  * Post username and an optional password
  * Returns an authentication token


- `POST /join/<room_id>/as/<token> -> JoinError`
  * Joins a given room as the current user
  * POST a password if needed


- `POST /create_room/ -> RoomId`
  * Creates a new private room
  * Optionally requires a password, to make a password protected room
  * Returns a room ID


- `POST /lobby/send_message/as/<token>`
  * Takes a plaintext message as an argument
  * Messages will be truncated to 200 characters


- `GET /lobby/messages -> Messages`
  * Returns all lobby messages


- `GET /lobby/messages/subscribe -> SubscriptionID`
  * Returns a subscription id


- `GET /lobby/new_messages/<subscription_id> -> Messages`
  * Returns new lobby messages



## Game API

- `GET /game/<room_id>/state/as/<token> -> GameState`
  * Gets the current game state
  * This only shows game state visible to the authed player
  * This will return a huge json object


- `GET /game/<room_id>/subscribe/as/<token> -> SubscriptionID`
  * Subscribe to new events in a room
  * This returns a subscription id that can be used to get new events from a
    game


- `GET /game/<room_id>/new_events/<subscription_id> -> [Event]`
  * Return all new events from the room
  * This shouldn't need auth because the subscription id is associated to
    specific users
  * As soon as events are returned they are marked read and will not show up in
    the subscription again
  * Clients are recommended to periodically fully sync the state of the game in
    case messages get dropped


- `POST /game/<room_id>/new_action/as/<token> -> ActionError`
  * Accepts a JSON object representing an action
  * And takes the given action in the game
  * Returns whether the action could successfully be taken


- `GET /game/<room_id>/kong/as/<token>`
- `GET /game/<room_id>/pung/as/<token>`
- `GET /game/<room_id>/chow/with/<card_id>/<card_id>/as/<token>`
- `GET /game/<room_id>/draw/as/<token>`
- `GET /game/<room_id>/mahjong/as/<token>`
- `GET /game/<room_id>/discard/<tile_number>/as/<token>`
  * Convenience wrappers around `new_action`
  * These also return an ActionError


- `POST /game/<room_id>/message/as/<token>`
  * Takes a plaintext message as an argument

